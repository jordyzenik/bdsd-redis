# Jedis voorbeeld
In deze repository staat een voorbeeld voor het gebruik van Jedis.
Jedis is een een Java client waarmee een Redis server kan worden benadert.
Er wordt gebruik gemaakt van de verschillende datastructuren die Redis ondersteunt.
Deze datastructuren zijn:
- String
- List
- Set
- HashSet
- Sorted set (zSet)
