import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class JedisExample {

    public static void main(String[] args) throws Exception {
        // Connecting to the redis server
        Jedis jedis = new Jedis("localhost", 6379);
        System.out.println("Connected to Redis");
        System.out.println("_________________________________________________");
        System.out.println();


        //String example
        System.out.println("STRING EXAMPLES:");
        System.out.println("Creating key: 'string' with value: 'testValue'");
        jedis.set("string", "testValue");
        System.out.println();

        System.out.println("Fetching value for key 'string'");
        System.out.println("Value for 'string' = " + jedis.get("string"));
        System.out.println();

        System.out.println("Deleting the value for 'string'");
        jedis.del("string");

        System.out.println("Fetching value for key 'string'");
        System.out.println(jedis.get("string"));
        System.out.println("_________________________________________________");
        System.out.println();


        //List example
        System.out.println("LIST EXAMPLES:");
        System.out.println("Creating list with values: 'value 1', 'value 2', 'value 3'");
        List<String> list = jedis.lrange("list", 0, -1);
        jedis.rpush("list", "value 1");
        jedis.rpush("list", "value 2");
        jedis.rpush("list", "value 3");

        System.out.println("Loop through list and print the values");
        list = jedis.lrange("list", 0, -1);
        for(String value: list){
            System.out.println(value);
        }
        System.out.println();

        System.out.println("Printing the value of list index 0");
        System.out.println(jedis.lindex("list", 0));
        System.out.println();

        System.out.println("Pop value from list");
        jedis.lpop("list");
        System.out.println("Loop through list and print the remaining values");
        list = jedis.lrange("list", 0, -1);
        for(String value: list){
            System.out.println(value);
        }
        System.out.println();

        System.out.println("Popping all values from list");
        list = jedis.lrange("list", 0, -1);
        for(String value: list){
            jedis.lpop("list");
        }

        System.out.println("Length of list after popping all values: " + jedis.llen("list"));
        System.out.println("_________________________________________________");
        System.out.println();


        //Set Example
        System.out.println("SET EXAMPLES");
        System.out.println("Adding values cheese taco, chicken taco, beef taco, cheese taco to the set");
        
        jedis.sadd("set", "cheese taco");
        jedis.sadd("set", "chicken taco");
        jedis.sadd("set", "beef taco");
        jedis.sadd("set", "cheese taco");

        System.out.println("Printing all values of the set, to show no double values are present");
        
        Set<String> set = jedis.smembers("set");
        for(String value : set){
            System.out.println(value);
        }
        System.out.println();

        System.out.println("Check if cheese taco is present in the set, 'if present: print value = present'");
        if(jedis.sismember("set", "cheese taco")){
            System.out.println("cheese taco = present");
        } else {
            System.out.println("cheese taco != present");
        }
        System.out.println();

        System.out.println("Remove cheese taco from the set");
        jedis.srem("set", "cheese taco");
        set = jedis.smembers("set");
        System.out.println("Printing all values of the set, to show cheese taco is removed");
        for(String value : set){
            System.out.println(value);
        }
        System.out.println("_________________________________________________");
        System.out.println();


        //HashSet Example
        System.out.println("HASHSET EXAMPLES");
        System.out.println();

        System.out.println("Adding a hashset with key: hash-key, values:  {key: key1, value: value1}, {key: key2, value: value2}");
        jedis.hset("hash-key", "key1", "value1");
        jedis.hset("hash-key", "key2", "value2");

        System.out.println("Printing all maps of the hashset");
        Map<String,String> map = jedis.hgetAll("hash-key");
        for (Map.Entry<String, String> entry : map.entrySet()){
            System.out.println("Entry Key: " + entry.getKey() +" Entry Value: " + entry.getValue());
        }
        System.out.println();

        System.out.println("Removing map with key: 'key1'");
        jedis.hdel("hash-key", "key1");
        map = jedis.hgetAll("hash-key");

        System.out.println("Printing all remaining maps of the hashset");
        for (Map.Entry<String, String> entry : map.entrySet()){
            System.out.println("Entry Key: " + entry.getKey() +" Entry Value: " + entry.getValue());
        }
        System.out.println("_________________________________________________");
        System.out.println();

        //Zset Example
        System.out.println("Sorted Set Example. Info: Set is sorted by the score each value of the set has");

        System.out.println("Adding values to the sorted set");
        jedis.zadd("zSet-Key", 1, "zValue-2");
        jedis.zadd("zSet-Key", 2, "zValue-1");
        jedis.zadd("zSet-Key", 3, "zValue-3");

        System.out.println("Printing all values of the set with minimum score of 0 and maximum score of 3");
        Set<Tuple> sortedSet = jedis.zrangeByScoreWithScores("zSet-Key", 0,3);
        for(Tuple tuple : sortedSet){
            System.out.println("Key: " + tuple.getElement() + " Score:" + tuple.getScore());
        }
        System.out.println();


        System.out.println("Printing all values of the set with minimum score of 0 and maximum score of 2");
        Set<Tuple> zScoreSet = jedis.zrangeByScoreWithScores("zSet-Key", 0,2);
        for(Tuple tuple : zScoreSet){
            System.out.println("Key: " + tuple.getElement() + " Score:" + tuple.getScore());
        }
    }
}
